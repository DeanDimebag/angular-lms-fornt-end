import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AvailableComponent } from './components/available/available.component';
import { BookInsertEditComponent } from './components/book-insert-edit/book-insert-edit.component';
import { BooksComponent } from './components/books/books.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { ReserveComponent } from './components/reserve/reserve.component';
import { UserBookComponent } from './components/user-book/user-book.component';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';

const routes: Routes = [
  {
    path:"", redirectTo:'/login', pathMatch:"full"
  },
  {
    path:"users", component:UsersComponent
  },
  {
    path:"users/create", component:UserCreateComponent
  },
  {
    path:"users/edit/:id", component:UserEditComponent
  },
  {
    path:"books", component:BooksComponent
  },
  {
    path:"book/edit/:id", component:BookInsertEditComponent
  },
  {
    path:"book/add", component:BookInsertEditComponent
  },
  {
    path:"userbook", component:AvailableComponent
  },
  {
    path:"status", component:UserBookComponent
  },
  {
    path:"reserve-book", component:ReserveComponent
  },
  {
    path:"login", component:LoginComponent
  },
  {
    path:"register", component:RegisterComponent
  },
  {
    path:"profile", component:ProfileComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
