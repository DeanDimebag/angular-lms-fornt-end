import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
//DatePicker Ngx-Bootstrap
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
//Modal Ngx-Bootstrap
import { ModalModule } from 'ngx-bootstrap/modal';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BooksComponent } from './components/books/books.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookInsertEditComponent } from './components/book-insert-edit/book-insert-edit.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserBookComponent } from './components/user-book/user-book.component';
import { AvailableComponent } from './components/available/available.component';
import { ReserveComponent } from './components/reserve/reserve.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { ProfileComponent } from './components/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserCreateComponent,
    NavbarComponent,
    BooksComponent,
    BookInsertEditComponent,
    UserEditComponent,
    UserBookComponent,
    AvailableComponent,
    ReserveComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    // {
    //   provide : HTTP_INTERCEPTORS,
    //   useClass :HttpInterceptorService,
    //   multi:true
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
