import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';
import { UserbookService } from 'src/app/services/userbook.service';
import { RestapiService } from 'src/app/services/restapi.service';
import { Router } from '@angular/router';
import { DatePipe, formatDate } from '@angular/common';

@Component({
  selector: 'app-available',
  templateUrl: './available.component.html',
  styleUrls: ['./available.component.scss']
})
export class AvailableComponent implements OnInit {
  form: FormGroup;
  books: Observable<any>;
  today = new Date();

  constructor(
    private readonly bookService: BooksService,
    private readonly userbookService: UserbookService,
    private restService: RestapiService,
    private fb: FormBuilder,
    private route: Router) { }

  ngOnInit(): void {
    let todayDate = formatDate(this.today, 'yyyy-MM-dd', 'en-US');
    console.log("Today Date : " + todayDate);

    let afterDate = new Date(this.today.setDate(this.today.getDate() + 10));
    let expireDate = formatDate(afterDate, 'yyyy-MM-dd', 'en-US');
    console.log(expireDate);

    this.books = this.bookService.availableBooks();
    console.log("Books list : " + this.books);

    this.form = this.fb.group({
      user: [''],
      book: [''],
      borrowDate: [todayDate],
      expireDate: [expireDate]
    });
  }

  bookNow(isbn) {
    let user = this.restService.isUserLoggedIn();
    console.log(user);
    if (user === false) {
      this.route.navigate(['/login']);
    } else {
      console.log(isbn);
      console.log("Email : " + this.restService.getLoggedEmail());
      let email = this.restService.getLoggedEmail();
      console.log("Password : " + this.restService.getLoggedPassword());
      let password = this.restService.getLoggedPassword();
      // let authentication = this.restService.createBasicAuthentication(email, password);
      let data = this.form.value;
      data.book = { isbn: isbn };
      data.user = { email: this.restService.getLoggedEmail() };
      console.log(data);
      this.userbookService.saveBook(data, email, password).subscribe(
        response => console.log("Success!", response),
        error => console.error("Error!", error)
      );
    }
  }

}
