import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookInsertEditComponent } from './book-insert-edit.component';

describe('BookInsertEditComponent', () => {
  let component: BookInsertEditComponent;
  let fixture: ComponentFixture<BookInsertEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookInsertEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookInsertEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
