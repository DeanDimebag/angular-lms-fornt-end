import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Book } from 'src/app/models/book';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-book-insert-edit',
  templateUrl: './book-insert-edit.component.html',
  styleUrls: ['./book-insert-edit.component.scss']
})
export class BookInsertEditComponent implements OnInit {

  // form = new FormGroup({
  //   isbn: new FormGroup(''),
  //   bookTitle: new FormGroup(''),
  //   authorName: new FormGroup(''),
  //   publication: new FormGroup('')
  // });

  id: number;
  header: string;
  books: Observable<Book[]>;
  form : FormGroup;
  here : boolean;
  updateMsg : boolean = false;

  constructor(private router: ActivatedRoute, private route : Router, private readonly bookService: BooksService, private readonly fb: FormBuilder) { }

  ngOnInit(): void {

    this.id = +this.router.snapshot.paramMap.get('id');
    console.log(this.id);
    console.log(!this.id);
    this.header = this.id === 0 ? 'Add new book' : 'Edit Book';

    this.id = this.router.snapshot.params['id'];
    console.log(this.id);
    this.here = !this.id;

    if(!this.here){
    this.bookService.fetchBook(this.id).subscribe(data => {
      console.log(data);
      this.form = this.fb.group({
        isbn: [data.isbn],
        bookTitle: [data.bookTitle],
        authorName: [data.authorName],
        publication: [data.publication]
      });
    })
  }

    this.form = this.fb.group({
      isbn: [''],
      bookTitle: [''],
      authorName: [''],
      publication: ['']
    });

  }


  GOTO() {
    this.route.navigate(['/books']);
  } 

  onSubmit() {
    if(this.here){
      this.bookService.saveBook(this.form.value).subscribe(
        response => console.log("Success!", response),
        error => console.error("Error!", error)
      )
      this.form.reset();
      this.GOTO();
    }else{
     
      console.log(this.form.value);
     
      this.bookService.updateBook(this.form.value).subscribe(
        response => console.log("Success!", response),
        error => console.error("Error!", error)
      );
      this.form.reset();
      this.GOTO();
    }
  }
}
