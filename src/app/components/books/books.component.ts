import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';
import { Book } from '../../models/book';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  books: Observable<Book[]>;
  constructor(private readonly bookService: BooksService, private readonly router: Router, private readonly route : ActivatedRoute) { }

  id: number;

  ngOnInit(): void {

    this.books = this.bookService.fetchBooks();
    console.log("List of Books : " + this.books);

  }

  delete(id){
    console.log(id);
    this.bookService.deleteBook(id).subscribe(
      response => console.log("Delete Succesfully", response),
      error => console.log("Error Deleting ", error)
    )
  }

}
