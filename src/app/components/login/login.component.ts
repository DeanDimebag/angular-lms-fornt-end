import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RestapiService } from 'src/app/services/restapi.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  user : any;

  constructor(private readonly fb: FormBuilder,
    private route : Router, 
    private restService : RestapiService,
    private readonly userService: UsersService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: [''],
      password: ['']
    });
  }
  // GOTO() {
  //   this.route.navigate(['/books']);
  // } 

  onSubmit() {

    console.log(this.form.value);
    let resp = this.restService.login(this.form.value);
    resp.subscribe(data=>{
      this.user = data;
      console.log("User : "+ this.user);
      this.route.navigate(['/profile']);
    })
    // this.GOTO();
  }

}
