import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestapiService } from 'src/app/services/restapi.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isLoggedIn =  false;
  isAdmin : string;

  constructor(private restapiService : RestapiService, private route : Router) { }

  ngOnInit(): void {
    console.log(this.restapiService.isUserLoggedIn());
    this.isLoggedIn = this.restapiService.isUserLoggedIn();
    console.log('MENU ->'+ this.isLoggedIn);
    this.isAdmin = this.restapiService.getLoggedRole();
    console.log("ROLE : "+this.isAdmin)
  }

  handleLogout(){
    this.restapiService.logout();
    this.route.navigate(['/login']);
  }
}
