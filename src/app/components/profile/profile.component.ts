import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfileService } from 'src/app/services/profile.service';
import { RestapiService } from 'src/app/services/restapi.service';
import { UserbookService } from 'src/app/services/userbook.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  name : string;
  email : string;
  role : string;
  books : Observable<any[]>;

  constructor(private restService : RestapiService,
    private profileService : ProfileService,
    private userBookService : UserbookService) { }

  ngOnInit(): void {
    let email = this.restService.getLoggedEmail();
    // console.log(email);

    this.getProfile();
    // console.log(this.getProfile())

    this.books = this.profileService.fetchAllReservedBooks(email);
    // console.log("Books : " +this.books);
    }

    getProfile(){
      let email = this.restService.getLoggedEmail();
      this.profileService.getProfileByEmail(email)
       .subscribe(response => {
        //  console.log(response);
         this.email = response.email;
         this.name = response.name;
         this.role = response.roles;
        //  console.log(this.email+", "+this.name+", "+this.role);
       });
    }

    delete(id){
      console.log("Id : " +id);
      console.log("Email : "+this.restService.getLoggedEmail());
      let email = this.restService.getLoggedEmail();
      console.log("Password : "+this.restService.getLoggedPassword());
      let password = this.restService.getLoggedPassword();
      const resp = this.userBookService.deleteBook(id, email, password);
      resp.subscribe(
       data =>{
         console.log(data);
        // this.getLatestData();
       });
    }
}
