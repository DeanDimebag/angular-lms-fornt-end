import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  constructor(private readonly fb: FormBuilder,
     private route : Router, 
     private readonly userService: UsersService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [''],
      name: [''],
      email: [''],
      password: [''],
      roles: [undefined]
    });
  }

  GOTO() {
    this.route.navigate(['/login']);
  } 

  onSubmit() {
    const val = this.form.value;
    val.roles = [{name:val.roles}];

    console.log(val);
    this.userService.saveuser(val).subscribe(
      response => console.log("Success!", response),
      error => console.error("Error!", error)
    );
    this.form.reset();
    this.GOTO();
  }

}
