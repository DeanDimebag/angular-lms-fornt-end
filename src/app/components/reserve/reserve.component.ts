import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';
import { UserbookService } from 'src/app/services/userbook.service';
import { UsersService } from 'src/app/services/users.service';
import { UsersComponent } from '../users/users.component';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.scss']
})
export class ReserveComponent implements OnInit {

  form: FormGroup;
  books: Observable<any>;
  users: any;


  constructor(
    private readonly fb: FormBuilder, 
    private userService: UsersService, 
    private bookService: BooksService,
    private readonly userBookService : UserbookService,
    private route : Router
    ) { }

  ngOnInit(): void {

    this.users = this.userService.fetchUsers();
    console.log(this.users);
    this.books = this.bookService.availableBooks();
    console.log(this.books);
    
    this.form = this.fb.group({
      id: [''],
      user: [{userId : ''}],
      book: [{isbn : ''}],
      borrowDate: ['2021-01-01'],
      expireDate: ['2021-02-01']
    });

  }

  // onSubmit() {
  //   const val = this.form.value;
  //   val.user = {userId:val.user};
  //   val.book = {isbn:val.book};
  //   console.log(this.form.value);
  //   this.userBookService.saveBook(val).subscribe(
  //     response => console.log("Success!", response),
  //       error => console.error("Error!", error)
  //   )
  //   this.GOTO();
  // }

  GOTO() {
    this.route.navigate(['/userbook']);
  } 

}
