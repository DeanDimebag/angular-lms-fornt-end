import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';
import { RestapiService } from 'src/app/services/restapi.service';
import { UserbookService } from 'src/app/services/userbook.service';

@Component({
  selector: 'app-user-book',
  templateUrl: './user-book.component.html',
  styleUrls: ['./user-book.component.scss']
})
export class UserBookComponent implements OnInit {

  books: Observable<any>;
  
  constructor(private readonly bookService: BooksService,
     private readonly userBookService : UserbookService,
     private restService : RestapiService) { }

  ngOnInit(): void {
    this.books = this.userBookService.reservedBooks();
    console.log(this.books);
  }

  getLatestData(){
    this.bookService.fetchBooks().
    subscribe((data) => this.books = data);
  }

  delete(id){
    console.log("Id : " +id);
    console.log("Email : "+this.restService.getLoggedEmail());
    let email = this.restService.getLoggedEmail();
    console.log("Password : "+this.restService.getLoggedPassword());
    let password = this.restService.getLoggedPassword();
    const resp = this.userBookService.deleteBook(id, email, password);
    resp.subscribe(
     data =>{
      //  console.log(data);
      this.getLatestData();
     });
  }

}
