import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  form: FormGroup;

  constructor(private readonly fb: FormBuilder, private route : Router, private readonly userService: UsersService) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      id: [''],
      name: [''],
      email: [''],
      password: [''],
      roles: [undefined]
    });

  }

  handleFormSubmit(): void {
    console.log(this.form?.value);
  }

  GOTO() {
    this.route.navigate(['/users']);
  } 

  onSubmit() {
    const val = this.form.value;
    val.roles = [{name:val.roles}];

    console.log(val);
    this.userService.saveuser(val).subscribe(
      response => console.log("Success!", response),
      error => console.error("Error!", error)
    );
    this.form.reset();
    this.GOTO();
  }

}
