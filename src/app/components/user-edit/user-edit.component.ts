import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  form: FormGroup;
  id : number;

  constructor(private readonly fb: FormBuilder, 
    private router: ActivatedRoute,private userService :UsersService,
    private route : Router) { }

  ngOnInit(): void {

    this.id = +this.router.snapshot.paramMap.get('id');
    console.log(this.id);
    console.log(!this.id);

    this.id = this.router.snapshot.params['id'];
    console.log(this.id);

    this.form = this.fb.group({
      userId: [''],
      name: [''],
      email: [''],
      password: ['']
    });

    this.userService.fetchUser(this.id).subscribe(data => {
      console.log(data);
      this.form = this.fb.group({
        userId: [data.userId],
        name: [data.name],
        email: [data.email],
        password: [data.password]
      });
    })
  }

  GOTO() {
    this.route.navigate(['/users']);
  } 

  onSubmit() {
    console.log(this.form.value);
    this.userService.updateuser(this.form.value).subscribe(
      response => console.log("Success!", response),
        error => console.error("Error!", error)
    )
    this.GOTO();
  }

}
