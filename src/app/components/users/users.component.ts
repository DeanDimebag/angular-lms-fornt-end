import { Observable } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';

import { Component, TemplateRef, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: Observable<any[]> | undefined;
  editUser: FormGroup;
  constructor(private readonly usersService: UsersService,
    private route : Router,
    private modalService: BsModalService, private readonly fb: FormBuilder) { }

  ngOnInit(): void {
    this.users = this.usersService.fetchUsers();

    this.editUser = this.fb.group({
      name: [''],
      address: ['']
    })
  }

  delete(id) {
    console.log(id);
    this.usersService.deleteUser(id).subscribe(
      response => console.log("Delete Succesfully", response),
      error => console.log("Error Deleting ", error)
    )
      this.allData();
  }

  allData(){
    this.usersService.fetchUsers().subscribe(
      response => console.log("Delete Succesfully", response),
      error => console.log("Error Deleting ", error)
    )
  }

  modalRef: BsModalRef;
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
