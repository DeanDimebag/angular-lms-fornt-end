export interface Book {
    isbn: string;
    bookTitle: string;
    authorName: string;
    publication: string;
}