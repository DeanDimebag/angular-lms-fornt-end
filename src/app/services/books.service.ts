import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Book } from '../models/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private apiServer = "http://localhost:9090";

  constructor(private readonly httpClient: HttpClient) { }

  //TO FETCH ALL LIST OF BOOKS
  fetchBooks(): Observable<any> {
    return this.httpClient.get(`http://localhost:9090/books`)
      .pipe(
        tap(() => console.log("Fetching Books List...")));
  }

  //TO FETCH SINALGE BOOK BY ISBN ID
  fetchBook(isbn : number): Observable<any> {
    return this.httpClient.get(this.apiServer+'/books/'+isbn)
      .pipe(
        tap(() => console.log("Fetching Books...")));
  }

  //TO SAVE BOOK
  saveBook(data){
    console.log("Inside service : "+data);
    return this.httpClient.post(`http://localhost:9090/books`, data);
  }

  //TO DELETE BOOK BY ISBN ID
  deleteBook(id)  {
    return this.httpClient.delete(`http://localhost:9090/books/delete/${id}`,{responseType: 'text'});
  }

  //TO UPDATE BOOK
  updateBook(data){
    return this.httpClient.put(`http://localhost:9090/books/update/${data.isbn}`, data);
  }

  //TO FETCH AVAILABLE BOOKS
  availableBooks(){
    return this.httpClient.get('http://localhost:9090/books/available');
  }

}
