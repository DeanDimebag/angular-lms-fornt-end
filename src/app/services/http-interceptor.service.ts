import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestapiService } from './restapi.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor{

  constructor(private restService : RestapiService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.restService.isUserLoggedIn() && req.url.indexOf('basicauth') === -1){
      const authReq = req.clone({
        headers : new HttpHeaders ({
          'Content-Type' : 'applicatio/json',
          'Authorization' : `Basic ${window.btoa(this.restService.email + ":"+ this.restService.password)}`
        })
      });
      return next.handle(authReq);
    }else{
      return next.handle(req);
    }
  }
}
