import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RestapiService } from './restapi.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
 
  loggedEmail : string;
  loggedPassword : string;

  constructor(private readonly httpClient: HttpClient,
    private restService : RestapiService) { }

  getProfileByEmail(email: string) : Observable<any> {
    this.loggedEmail = this.restService.getLoggedEmail();
    this.loggedPassword = this.restService.getLoggedPassword();
    // console.log("Email : "+this.loggedPassword+ ", Password : "+this.loggedEmail);
    const headers = new HttpHeaders({ Authorization: this.restService.createBasicAuthentication(this.loggedEmail, this.loggedPassword), "Email": this.loggedEmail });
    return this.httpClient.get('http://localhost:9090/profile', { headers }).pipe(
      tap(() => console.log("Fetching User Profile...")));
    }

    fetchAllReservedBooks(email: string): Observable<any> {
      this.loggedEmail = this.restService.getLoggedEmail();
    this.loggedPassword = this.restService.getLoggedPassword();
    // console.log("Email : "+this.loggedPassword+ ", Password : "+this.loggedEmail);
    const headers = new HttpHeaders({ Authorization: this.restService.createBasicAuthentication(this.loggedEmail, this.loggedPassword), "Email": this.loggedEmail });
    return this.httpClient.get('http://localhost:9090/profile/books', { headers}).pipe(
      tap(() => console.log("Fetching Reserved Books List...")));
    }
  

}
