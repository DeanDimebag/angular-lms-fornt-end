import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestapiService {

  USERNAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
  USERNAME_SESSION_ATTRIBUTE_PASSWORD = 'authenticatedPassword';
  USERNAME_SESSION_ATTRIBUTE_ROLE = 'authenticatedRole';

  email: string;
  password: string;
  role:string;

  constructor(private httpClient: HttpClient) { }

  // login (data){
  //   const headers  = new HttpHeaders({Authorization : 'Basic ' + btoa(data.email+":"+data.password)})
  //   return this.httpClient.get("http://localhost:9090/basic" , {headers , responseType:'text' as 'json'})
  // }

  login(data) {
    this.email = data.email;
    this.password = data.password;
    const headers = new HttpHeaders({ Authorization: this.createBasicAuthentication(data.email, data.password), "Email": this.email });
    return this.httpClient.get("http://localhost:9090/basic", { headers })
      .pipe(map((res : any) => {
        this.email = res.email;
        // console.log("Email : "+this.email);
        this.password = data.password;        
        // console.log("Password : "+this.password);
        this.role = res.roles;
        // console.log("Role : "+this.role);
        this.loginSuccessful(this.email, this.password, this.role);
      }
      // { 
      //   console.log(res);
      //   this.email = data.email;
      //   this.loginSuccessful(this.email, this.password);
      // }
      )
      );
  }

  createBasicAuthentication(email: string, password: string) {
    return 'Basic ' + window.btoa(email + ":" + password);
  }

  loginSuccessful(email, password, role) {
    sessionStorage.setItem(this.USERNAME_SESSION_ATTRIBUTE_NAME, email);
    sessionStorage.setItem(this.USERNAME_SESSION_ATTRIBUTE_PASSWORD, password);
    sessionStorage.setItem(this.USERNAME_SESSION_ATTRIBUTE_ROLE, role);
  }

  logout() {
    sessionStorage.removeItem(this.USERNAME_SESSION_ATTRIBUTE_NAME);
    this.email == null;
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(this.USERNAME_SESSION_ATTRIBUTE_NAME);
    // console.log("isLoggedInUser => " + user);
    if (user == null) return false;
    return true;
  }

  getLoggedEmail() {
    let user = sessionStorage.getItem(this.USERNAME_SESSION_ATTRIBUTE_NAME);
    // console.log("isLoggedEmail => " + user);
    if (user == null) return ''
    return user;
  }

  getLoggedPassword() {
    let user = sessionStorage.getItem(this.USERNAME_SESSION_ATTRIBUTE_PASSWORD);
    // console.log("isLoggedPassword => " + user);
    if (user == null) return ''
    return user;
  }

  getLoggedRole() {
    let user = sessionStorage.getItem(this.USERNAME_SESSION_ATTRIBUTE_ROLE);
    // console.log("isLoggedPassword => " + user);
    if (user == null) return ''
    return user;
  }

}
