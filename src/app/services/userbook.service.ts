import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RestapiService } from './restapi.service';

@Injectable({
  providedIn: 'root'
})
export class UserbookService {

  loggedEmail : string;
  loggedPassword : string;

  constructor(private readonly httpClient: HttpClient,
    private restapiService : RestapiService) { }

    //TO RESERVE AVAILABLE BOOK 
  saveBook(data, email, password){
    const headers = new HttpHeaders({ Authorization: this.restapiService.createBasicAuthentication(email, password) });
    return this.httpClient.post(`http://localhost:9090/bookuser`, data, { headers, responseType: 'text' as 'json' });
  }

  //TO RETURN THE AVAILABLE BOOK
  deleteBook(id, email, password)  {
    const headers = new HttpHeaders({ Authorization: this.restapiService.createBasicAuthentication(email, password) });
    return this.httpClient.delete(`http://localhost:9090/bookuser/delete/${id}`, { headers, responseType: 'text' as 'json' });
  }

  reservedBooks(): Observable<any>{
    return this.httpClient.get('http://localhost:9090/bookuser/reserved');
  }

}
