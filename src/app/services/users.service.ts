import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
 

  constructor(private readonly httpClient: HttpClient) { }

  fetchUsers() : Observable<any> {
    return this.httpClient.get('http://localhost:9090/users')
    .pipe(
      tap(() => console.log("Fetching Users Data......"))
    )
  }

  saveuser(data) {
    return this.httpClient.post(`http://localhost:9090/register`, data);
  }

  fetchUser(id : number): Observable<any> {
    return this.httpClient.get(`http://localhost:9090/users/`+id)
      .pipe(
        tap(() => console.log("Fetching User...")));
  }

  updateuser(data) {
    return this.httpClient.put(`http://localhost:9090/users/update`, data);
  }

  deleteUser(id){
    return this.httpClient.delete(`http://localhost:9090/users/delete/`+id,{responseType: 'text'});
  }
  
}
